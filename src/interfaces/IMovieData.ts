export interface MovieData {
  title: string;
  poster_path: string;
  overview: string;
  release_date: string;
  popularity: number;
  vote_average: number
}