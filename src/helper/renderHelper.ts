import {MovieData} from '../interfaces/IMovieData';
import { movies_count_per_page, movies_count_to_fetch, poster_img_url } from '../constants/constants';
import { fetchMovieInfo } from './apiHelper';

let pag_index = 1
let renderedCount = 0;

export const movie_list:MovieData[] = []

export const film_container:any = document.getElementById('film-container')

export function renderMovieCard (movie_data:MovieData){
  return `<div class="col-lg-3 col-md-4 col-12 p-2">
      <div class="card shadow-sm">
        <img
          src=${poster_img_url + movie_data.poster_path}
        />
        <svg
          xmlns="http://www.w3.org/2000/svg"
          stroke="red"
          fill="red"
          width="50"
          height="50"
          class="bi bi-heart-fill position-absolute p-2"
          viewBox="0 -2 18 22"
        >
          <path
            fill-rule="evenodd"
            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
          />
        </svg>
        <div class="card-body">
          <p class="card-text truncate">
            ${movie_data.overview}
          </p>
          <div
            class="
              d-flex
              justify-content-between
              align-items-center
            "
          >
            <small class="text-muted">${movie_data.release_date}</small>
          </div>
        </div>
      </div>`
}

export function createMoviesList(){
  for (let i = 0; i < movies_count_to_fetch; i++) {
      fetchMovieInfo(i)
  }
}

const top_rate = document.getElementById('top_rated')
if (top_rate){
  top_rate.addEventListener('click', ()=> {
    movie_list.sort((a, b) => {
      return a.vote_average > b.vote_average ? -1 : 1
    })
    console.log(movie_list);
    film_container.innerHTML = ''
    renderAllMovies()
  })
}

const popular = document.getElementById('popular')
if (popular){
  popular.addEventListener('click', ()=> {
    movie_list.sort((a, b) => {
      return a.popularity > b.popularity ? -1 : 1
    })
    film_container.innerHTML = ''
    renderAllMovies()
  })
}

// const upcoming = document.getElementById('upcoming')
// if (upcoming){
//   upcoming.addEventListener('click', () => {
//     movie_list.map((data)=> {
//       const date_movie = new Date(data.release_date)
//       const now_date = new Date()
//       console.log(date_movie > now_date);
//       if(date_movie > now_date){
//         return data
//       }
//     })
//     console.log(movie_list);
//   })
// }load-more

const load_more = document.getElementById('load-more')
if (load_more){
  load_more.addEventListener('click', () => {
    renderMoreMovies()
    pag_index++
  })
}

function renderAllMovies (){
  for (let i = 0; i < movies_count_per_page * pag_index; i++) {
    film_container.insertAdjacentHTML('beforeend', renderMovieCard(movie_list[i]))
  }
}

function renderMoreMovies() {
  for (let i = pag_index * movies_count_per_page; i < (pag_index + 1) * movies_count_per_page; i++) {
    film_container.insertAdjacentHTML('beforeend', renderMovieCard(movie_list[i]))

    renderedCount++
  }

  console.log(renderedCount)
}

// export async function createMoviesList(){
//   const promises = [];
//
//   for (let i = 0; i < movies_count_per_page; i++) {
//     const promise = new Promise((resolve, reject) => {
//       fetchMovieInfo(i)
//     })
//     promises.push(promise)
//   }
//
//   return Promise.allSettled(promises).then(results => console.log(results))
// }

// export function renderMoviesList() {
//   for (let i = 0; i < movies_count_per_page; i++) {
//     film_container.insertAdjacentHTML('beforeend', renderMovieCard(movie_list[i]))
//   }
// }

