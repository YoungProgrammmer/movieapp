import { MovieData } from '../interfaces/IMovieData';
import { api_key, api_url, movies_count_per_page } from '../constants/constants';
import { film_container, movie_list, renderMovieCard } from './renderHelper';

export async function api<T>(url: string): Promise<T> {
    return fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }

            return response.json() as Promise<T>
        })
}

export function fetchMovieInfo(id:number){
  api<MovieData>(`${api_url + (101 + id) + '?api_key=' + api_key}`)
    .then((data) => {
      if(data.poster_path)
      {
        movie_list.push(data)

        if(id < movies_count_per_page) {
          film_container.insertAdjacentHTML('beforeend', renderMovieCard(data))
        }
      }
    })
    .catch(error => {
    })
}

